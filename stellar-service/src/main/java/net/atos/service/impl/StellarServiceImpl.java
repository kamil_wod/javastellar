package net.atos.service.impl;

import net.atos.dao.StarDAO;
import net.atos.model.Star;
import net.atos.service.StellarService;
import net.atos.dao.ConstellationDAO;
import net.atos.model.Constellation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Primary
@Transactional
public class StellarServiceImpl implements StellarService {

    @Autowired
    ConstellationDAO constellationDAO;

    @Autowired
    StarDAO starDAO;

    @Override
    public List<Constellation> getConstellations(String query) {

        List<Constellation> constellations = new ArrayList<Constellation>();

        if (StringUtils.isEmpty(query)) {
            constellations = constellationDAO.getAllConstellations();
        } else {
            constellations = constellationDAO.getConstellations(query);
        }

        return constellations;

    }

    @Override
    public List<Star> getStarsForConstellation(String constellationAbbreviation) {
        return starDAO.getStarsForConstellation(constellationAbbreviation);
    }

    @Override
    public Constellation addConstellation(Constellation constellation) {
        return constellationDAO.addConstellation(constellation);
    }

    @Override
    public Star addStar(Star star) {
        return starDAO.addStar(star);
    }

}
