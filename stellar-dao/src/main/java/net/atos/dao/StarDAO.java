package net.atos.dao;

import net.atos.model.Constellation;
import net.atos.model.Star;

import java.util.List;


public interface StarDAO {

    List<Star> getStarsForConstellation(String constellationAbbreviation);

    Star addStar(Star star);
}
