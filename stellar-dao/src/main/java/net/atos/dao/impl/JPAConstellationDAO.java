package net.atos.dao.impl;

import net.atos.dao.ConstellationDAO;
import net.atos.model.Constellation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JPAConstellationDAO implements ConstellationDAO {

    @PersistenceContext(name="stellar")
    private EntityManager em;

    @Override
    public List<Constellation> getAllConstellations() {
        return em.createQuery("SELECT c FROM Constellation c ").getResultList();
    }

    @Override
    public List<Constellation> getConstellations(String query) {
        return em.createQuery("SELECT c FROM Constellation c WHERE lower(c.name) LIKE :param")
                .setParameter("param", "%" + query.toLowerCase()+ "%")
                .getResultList();
    }


    @Override
    public Constellation addConstellation(Constellation constellation) {
        em.persist(constellation);
        return constellation;
    }

}
