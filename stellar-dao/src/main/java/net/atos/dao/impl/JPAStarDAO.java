package net.atos.dao.impl;

import net.atos.dao.StarDAO;
import net.atos.model.Constellation;
import net.atos.model.Star;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JPAStarDAO implements StarDAO{

    @PersistenceContext(name="stellar")
    private EntityManager em;


    @Override
    public List<Star> getStarsForConstellation(String constellationAbbreviation) {
        return em.createQuery("SELECT s FROM Star s WHERE s.constellation.abbreviation LIKE :param")
                .setParameter("param", constellationAbbreviation)
                .getResultList();
    }

    @Override
    public Star addStar(Star star) {
        em.persist(star);
        return star;
    }
}
