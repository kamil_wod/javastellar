package net.atos.dao;

import net.atos.model.Constellation;
import java.util.List;

public interface ConstellationDAO {

    List<Constellation> getAllConstellations();

    List<Constellation> getConstellations(String query);

    Constellation addConstellation(Constellation constellation);
}
