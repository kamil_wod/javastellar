package net.atos.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Constellation {

    @Id
    @Column
    private String abbreviation;

    @Column
    private String name;

    @OneToMany(mappedBy = "constellation", fetch = FetchType.EAGER)
    Collection<Star> stars = new ArrayList<Star>();

    public Constellation() {}

    public Constellation(String abbreviation, String name) {
        this.abbreviation = abbreviation;
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Constellation{" +
                ", abbreviation='" + abbreviation + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
