package net.atos;

import net.atos.model.Star;
import net.atos.service.StellarService;
import net.atos.model.Constellation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ConstellationRESTController {

    @Autowired
    StellarService stellarService;

    @GetMapping("/constellations")
    public List<Constellation> getAllConstellations(@RequestParam(value="query", required=false) String query) {
        return stellarService.getConstellations(query);
    }

    @RequestMapping(value="/constellations/{constellationAbbreviation}/stars", method = RequestMethod.GET)
    public List<Star> getStarsForConstellation(@PathVariable("constellationAbbreviation") String constellationAbbreviation) {
        return stellarService.getStarsForConstellation(constellationAbbreviation);
    }

    @RequestMapping(value = "/constellation", method = RequestMethod.POST)
    public ResponseEntity addConstellation(Constellation constellation) {
        Constellation newConstellation = stellarService.addConstellation(constellation);
        return new ResponseEntity<>(newConstellation, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/star", method = RequestMethod.POST)
    public ResponseEntity addStar(Star star) {
        Star newStar = stellarService.addStar(star);
        return new ResponseEntity<>(newStar, HttpStatus.CREATED);
    }

}
